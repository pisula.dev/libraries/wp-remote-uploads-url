<?php

namespace IC\Functionality;

class RemoteUploads {

	/**
	 * @var string
	 */
	private string $url;

	/**
	 * @param string $url .
	 */
	public function __construct( string $url ) {
		$this->url = $url;
	}

	/**
	 * @return void
	 */
	public function hooks(): void {
		add_filter( 'mod_rewrite_rules', [ $this, 'mod_rewrite_rules' ] );
	}

	/**
	 * Filter the list of rewrite rules formatted for output to an .htaccess file.
	 *
	 * @param mixed $rules mod_rewrite Rewrite rules formatted for .htaccess.
	 * @url http://stevegrunwell.github.io/wordpress-git/#/13
	 *
	 * @return string
	 */
	public function mod_rewrite_rules( $rules ): string {
		return $this->get_new_rules() . "\n\n" . $rules;
	}

	/**
	 * @return string
	 */
	private function get_new_rules(): string {
		$uploads = wp_get_upload_dir();

		return sprintf(
			'<IfModule mod_rewrite.c>
	RewriteEngine on
	RewriteCond %%{REQUEST_FILENAME} !-d
	RewriteCond %%{REQUEST_FILENAME} !-f
	RewriteRule %s/(.*) %s/$1 [NC,L]
</IfModule>',
			str_replace( trailingslashit( home_url() ), '', $uploads['baseurl'] ),
			str_replace( trailingslashit( home_url() ), trailingslashit( $this->url ), $uploads['baseurl'] ),
		);
	}
}
